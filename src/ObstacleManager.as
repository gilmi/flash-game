﻿package  {
	
	import flash.display.MovieClip;
	
	public class ObstacleManager extends MovieClip {
		
		var lastTime;
		var paren;
		public function ObstacleManager(paren)
		{
			this.paren = paren;
			lastTime = 0;
		}
		public function createObstacle(time)
		{
			if (time - lastTime > 80 + (Math.random() * 200))
			{
				lastTime = time;
				var obs;
				var r = Math.random() * 4;
				if (r < 2)                  obs = new police_car();
				else if (2 <= r && r < 4)  obs = new Spike();
				obs.x = 20 + (Math.random() * (paren.width - obs.width - 20));
				return obs;
			}
			return null;
		}
	}
	
}
