﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.ui.Keyboard;
	
	public class MyCar extends MovieClip {
		
		var stg;
		var max_right;
		var max_down;
		public function MyCar(stg, p)
		{
			this.stg = stg;
			stg.addEventListener(KeyboardEvent.KEY_DOWN, moveCar);
			addEventListener(Event.REMOVED_FROM_STAGE, clean);
			
			max_down = 1000;
			max_right = p.width;
			
			x = 100;
			y = max_down - height - 150;
		}

		public function moveCar(e)
		{
			if(e.keyCode==Keyboard.LEFT)
			{
				 if(this.x >= 20)
					this.x -= 20;					
			}
			if(e.keyCode==Keyboard.RIGHT)
			{
				 if (this.x + this.width + 20 <= max_right)
					this.x += 20;
			}
			if(e.keyCode==Keyboard.UP)
			{
				 if(this.y >= 20)
					this.y-= 20;
			}
			if(e.keyCode==Keyboard.DOWN)
			{
				 if(this.y + this.height <= max_down)
					this.y+= 20;
			}

		} 

		function clean(e)
		{
			stg.removeEventListener(KeyboardEvent.KEY_DOWN, moveCar);
		}
	}
	
}
