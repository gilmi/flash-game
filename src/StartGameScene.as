﻿package 
{
	import flash.display.MovieClip;
	import flash.events.*;
	

	
	public class StartGameScene extends MovieClip
	{
		var scener : Scener;
		var stg;
		var btn;
		public function StartGameScene(stg, scener : Scener)
		{
			this.stg = stg;
			trace("StartGameScene");
			this.scener = scener;
			createStartAnimation();
			createStartButton();
			
		}
		function createStartAnimation()
		{
			var startAnimation = new startScrn1();
			startAnimation.x += 250;
			startAnimation.y += 600;
			addChild(startAnimation);
		}
		function createStartButton()
		{
			btn = new btnStart();
			btn.x = 250;
			btn.y = 850;
			btn.addEventListener(MouseEvent.MOUSE_DOWN, startGameClickHandler);
			addChild(btn);
		}
		public function startGameClickHandler(event)
		{
			btn.removeEventListener(MouseEvent.MOUSE_DOWN, startGameClickHandler);
			gotoPlayScene();
		}
		
		function gotoPlayScene()
		{
			var playScene = new PlayGameScene(stg,scener);
			scener.switchScene(this, playScene);
		}
	}
}
