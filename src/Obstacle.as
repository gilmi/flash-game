﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.*;
	
	public class Obstacle extends MovieClip {
				
		var accumulatedTime;
		public function Obstacle()
		{			
			accumulatedTime = 1;
			
			addEventListener(Event.ENTER_FRAME, moveObstacle);
			addEventListener(Event.REMOVED_FROM_STAGE, clean);
		}
		public function updateAccumulatedTime(time)
		{
			accumulatedTime = time;
		}
		function moveObstacle(e)
		{
			y += 10 + (accumulatedTime/100);
			if (y > 1500)
			{
				parent.removeChild(this);
			}
		}
		public function clean(e)
		{
			removeEventListener(Event.ENTER_FRAME, moveObstacle);
		}		
	}
	
}
