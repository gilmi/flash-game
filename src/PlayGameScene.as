﻿package 
{
	import flash.display.MovieClip;
	import flash.utils.Timer;
	import flash.events.*;
	
	public class PlayGameScene extends MovieClip
	{
		var timer : Timer;
		var scener : Scener;
		var stg;
		
		var myCar;
		var obstacles : Vector.<Obstacle>;
		var obsManager;
		var road;
		public function PlayGameScene(stg, scener : Scener)
		{
			this.stg = stg;
			trace("PlayGameScene");
			this.scener = scener;
			
			addRoad();
			addCar();
			obsManager = new ObstacleManager(this);
			
			obstacles = new Vector.<Obstacle>();
			addEventListener(Event.ENTER_FRAME, update);
			addEventListener(Event.REMOVED_FROM_STAGE, clean);
			
			timer = new Timer(1);
			timer.start();
		}
		function addRoad()
		{
			road = new MyRoad();
			addChild(road);
		}
		function addCar()
		{
			myCar = new Car(stg, this);
			addChild(myCar);
		}
		function update(e)
		{
			var obs = obsManager.createObstacle(timer.currentCount);
			if (obs != null)
			{
				obstacles.push(obs);
				addChild(obs);
				trace(obs);

			}
			road.updateAccumulatedTime(timer.currentCount);
			for (var i = 0; i < obstacles.length; i++)
			{
				obstacles[i].updateAccumulatedTime(timer.currentCount);
			}
			hitTestCarObs();
			
		}
		function hitTestCarObs()
		{
			for (var i = 0; i < obstacles.length; i++)
			{
				if (obstacles[i].hitTestObject(myCar))
				{
					
					var expl = new explosion();
					expl.x = 20 + myCar.x;
					expl.y = 50 + myCar.y;
					addChild(expl);
					clean(expl);

					var endTimer:Timer = new Timer(1000);
					endTimer.addEventListener(TimerEvent.TIMER, function (e) {
										   timer.stop();
										   gotoEndScene();
										   endTimer.stop();
									});
					endTimer.start();
					//gotoEndScene();
				}
			}	
		}

		function clean(e)
		{
			removeEventListener(Event.ENTER_FRAME, update);
			road.clean(e);
			for (var i = 0; i < obstacles.length; i++)
			{
				obstacles[i].clean(e);
			}
		}
		function gotoEndScene()
		{
			removeEventListener(Event.ENTER_FRAME, update);
			var endScene = new EndGameScene(stg, scener, timer.currentCount);
			scener.switchScene(this, endScene);
		}
	}
}
