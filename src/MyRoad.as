﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.*;
	
	
	public class MyRoad extends MovieClip {
		
		var road1;
		var road2;
		var accumulatedTime;
		public function MyRoad()
		{
			road1 = new Road();
			road1.y = -1000;
			road2 = new Road();
			road2.y = 0;
			
			accumulatedTime = 1;
			
			addChild(road1);
			addChild(road2);
			
			addEventListener(Event.ENTER_FRAME, moveRoads);
			addEventListener(Event.REMOVED_FROM_STAGE, clean);
		}
		public function updateAccumulatedTime(time)
		{
			accumulatedTime = time;
		}
		public function clean(e)
		{
			removeEventListener(Event.ENTER_FRAME, moveRoads);
		}
		function moveRoads(e)
		{
			//trace("roads moving");
			road1.y += 10 + (accumulatedTime / 100);
			road2.y += 10 + (accumulatedTime / 100);
			if (road1.y >= 0)
			{
				road1.y = -1000;
				road2.y = 0;
			}
		}
	}
	
}
