﻿package  {
	
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.events.MouseEvent;

	
	public class EndGameScene extends MovieClip
	{
		var score : int = 0;
		var scener : Scener;
		var stg;
		var btn;
		public function EndGameScene(stg, scener : Scener, score : int)
		{
			this.stg = stg;
			trace("EndGameScene");
			this.scener = scener;
			this.score = score;
			
			createText();
			createBG();
			createRetryButton();
			
			//addEventListener(MouseEvent.CLICK, mouseClickHandler);
		}
		function createText()
		{
			
			var scoreTextField = new Score();
			scoreTextField.x = 50;
			scoreTextField.y = 700;
			scoreTextField.scoreText.text = score;
			trace(scoreTextField.scoreText.text);
			
			
			addChild(scoreTextField);
		}
		function createBG()
		{
			var bg = new endScrn();
			bg.y += 100;
			addChild(bg);
		}
		function createRetryButton()
		{
			btn = new btnRetry();
			btn.x = 125;
			btn.y = 850;
			btn.addEventListener(MouseEvent.MOUSE_DOWN, mouseClickHandler);
			addChild(btn);
		}
		function mouseClickHandler(event:MouseEvent):void
		{
			btn.removeEventListener(MouseEvent.CLICK, mouseClickHandler);			
			gotoPlayScene();
		}
		function gotoPlayScene()
		{
			var playScene = new PlayGameScene(stg, scener);
			scener.switchScene(this, playScene);
		}
	}
	
}
